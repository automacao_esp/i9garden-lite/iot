# i9Garden Lite

 Sistema para irrigação de hortas ou jardins residenciais.
 Neste projeto será utilizado deepSleep para otimizar os gastos de energia e watchDog para evitar o travamento
 
 Material utilizado neste projeto :
  *  1 x Relé
  *  1 x regulador de tensão dc-dc   12V - 5v
  *  1 x fonte 220-110V para 12v
  *  1 x Módulo NodeMCU ESP8266
  *  1 x relógio DS1307
  *  1 x válvula solenoíde
  
  Referências:
  * deepSleep: https://randomnerdtutorials.com/esp8266-deep-sleep-with-arduino-ide/
  * watchDog : https://portal.vidadesilicio.com.br/watchdog-esp8266/
  
 

