/* 
 *  Programa: Sistema para irrigação automática
 *  Autor: Mayko Rodrigues Vieira
 */

//Carregando as bibliotecas necessárias para o projeto
#include <DS1307.h> 
#include <FS.h>     
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>
#include <DHT.h>

//Modulo RTC DS1307 ligado as portas A4 e A5 do Arduino
#define SDA 4    //D2 
#define SCL 5    //D1

//Definindo a porta apra o RELE
#define RELE 12  //D6


const String FILE_PATH_HORARIOS = "/garden_horarios.txt";
const char *ssid = "i9Garden";
const char *password = "12345678";

StaticJsonDocument<1024> doc;
JsonArray HORARIOS = doc.to<JsonArray>();

DS1307 rtc(SDA, SCL);        //relógio
ESP8266WebServer server(80); //servidor http na porta 80

unsigned long tempoDeIrrigacaoMillis = 0;

void setup()
{
  Serial.begin(115200);
  Serial.println("i9Garden start...");
  pinMode(RELE, OUTPUT);  
  delay(2000);

  ESP.wdtDisable();//Desabilita o SW WDT. 
  
  configurarRelogio();        //configurando o relógio
  
  SPIFFS.begin(); 
  delay(500);
  carregarAgendaDeIrrigacao();
  configurarWiFiAcessPoint(); // configurando o wifi como hotspot
  iniciarHttpServer();        // habilitando o http server
 
}

// processando fica em loop executando as rotinas programadas...
void loop()
{
  server.handleClient();  
  Serial.print("Hora : ");
  Serial.println(rtc.getTimeStr());  

  //esta rotina foi desenvolvida para evitar o travamento no delay ao irrigar...
  if(tempoDeIrrigacaoMillis > 0){ //sistema de irrigação ativo    
    if( millis() >= tempoDeIrrigacaoMillis){
      tempoDeIrrigacaoMillis = 0;
      digitalWrite(RELE, LOW); //desligo a irrigação      
    }    
  }else{
    irrigarPlantacao(horaDeIrrigar(rtc.getTimeStr()));  
  }

  delay(1000);   //Aguarda 1 segundo e repete o processo
  ESP.wdtFeed(); //Alimenta o Watchdog.
}


void configurarRelogio()
{
  rtc.halt(false);
  //As linhas abaixo setam a data e hora do modulo e podem ser comentada apos a primeira utilizacao
  //rtc.setDOW(FRIDAY);        //Define o dia da semana
  //rtc.setTime(19, 58, 00);   //Define o horario
  //rtc.setDate(14, 04, 2020); //Define o dia, mes e ano

  rtc.setSQWRate(SQW_RATE_1); //Definicoes do pino SQW/Out
  rtc.enableSQW(true);
}

void configurarWiFiAcessPoint()
{
  delay(5000);
  Serial.print("Configurando o access point...");
  WiFi.softAP(ssid, password);
  Serial.print("IP address: ");
  Serial.println(WiFi.softAPIP());
}

void iniciarHttpServer()
{
   //rotas utilizadas na aplicação i9Garden...
  server.on("/start", iniciarIrrigacaoManual);
  server.on("/stop", pararIrrigacaoManual);
  server.on("/get", statusDaIrrigacao);
  server.on("/get/agenda", getHorariosDeIrrigacao);
  server.on("/set/agenda", setHorariosDeIrrigacao);
  server.on("/get/clock", getHoraDoRelogio);
  server.on("/set/clock", setHoraDoRelogio);

  server.begin();
}

void irrigarPlantacao(int tempoDeIrrigacao)
{
  if (tempoDeIrrigacao > 0)
  {
    Serial.println("Hora de irrigar :)");
    digitalWrite(RELE, HIGH);
    tempoDeIrrigacaoMillis = millis() + (tempoDeIrrigacao * 60 * 1000);
  }
}

int horaDeIrrigar(String hora)
{
  int tempoDeIrrigacao = 0;
  for (JsonObject item : doc.as<JsonArray>())
  {
    if (item["hora"].as<String>() == hora)
    {
      tempoDeIrrigacao = item["tempo"].as<int>(); // tempo em minutos
      break;
    };
  }
  return tempoDeIrrigacao;
}

boolean gravarHorariosNoArquivo()
{
  //Abre o arquivo para escrita ("w" write) ("a" append)  
  //formas de escrita : https://arduino-esp8266.readthedocs.io/en/2.5.2/filesystem.html#open
  boolean sucesso = false;
  File file = SPIFFS.open(FILE_PATH_HORARIOS, "w");
  if (!file)
  {
    Serial.println("Erro ao abrir arquivo!");
  }
  else
  {
    if (serializeJson(HORARIOS, file) == 0)
      Serial.println("Failed to write to file");
  }
  file.close();

  return sucesso;
}

/* Carregando os horários do arquivo (banco de dados) */
void carregarAgendaDeIrrigacao()
{
  if (!SPIFFS.begin())
  {
    setarHorarioPadrao();
  }
  else
  {
    //SPIFFS.remove(FILE_PATH_HORARIOS);
    if (!SPIFFS.exists(FILE_PATH_HORARIOS))
    {
      Serial.println("Arquino inexistente");
      setarHorarioPadrao();
      gravarHorariosNoArquivo();
    }
    else
    {
      File file = SPIFFS.open(FILE_PATH_HORARIOS, "r");
      if (!file)
      {
        Serial.println("Erro ao abrir arquivo!");
        setarHorarioPadrao();
      }
      else
      {
        Serial.println("Arquino de agenda existe....");
        DeserializationError error = deserializeJson(doc, file);
        if (error)
        {
          Serial.println("Falha ao ler o arquivo, usando a configuração padrão");
          setarHorarioPadrao();
        }
        else
        {
          Serial.println("Horários carregados com sucesso....");
          serializeJson(HORARIOS, Serial);
        }
      }
      file.close();
    }
  }
}

void setarHorarioPadrao()
{  
  JsonArray json = doc.to<JsonArray>();
  JsonObject item = json.createNestedObject();
  item["hora"] = "06:00:00";
  item["tempo"] = "10";

  item = json.createNestedObject();
  item["hora"] = "18:00:00";
  item["tempo"] = "10";

  serializeJson(doc, Serial);
}

void iniciarIrrigacaoManual()
{
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  digitalWrite(RELE, HIGH);
  delay(100);

  DynamicJsonDocument json(200); 
  json["sucesso"] = true;
  json["msg"] = "Iniciando irrigação manual";
  String sendMessage = "";
  serializeJson(json, sendMessage);  
  server.send(200, "application/json",  sendMessage);
}

void pararIrrigacaoManual()
{
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  digitalWrite(RELE, LOW);
  tempoDeIrrigacaoMillis = 0;
  
  DynamicJsonDocument json(200); 
  json["sucesso"] = true;
  json["msg"] = "Parando irrigação manual";
  String sendMessage = "";
  serializeJson(json, sendMessage);  
  server.send(200, "application/json",  sendMessage);
}

void statusDaIrrigacao(){
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "GET");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  int status = digitalRead(RELE);
  DynamicJsonDocument json(200); 
  json["sucesso"] = true;
  json["status"] =  (status == HIGH) ? true : false ;
  String sendMessage = "";
  serializeJson(json, sendMessage);    
  server.send(200, "application/json",  sendMessage);
}


void getHoraDoRelogio(){ 
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "GET");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  DynamicJsonDocument json(200); 
  json["sucesso"] = true;
  json["msg"] = (String)rtc.getTimeStr();
  String sendMessage = "";
  serializeJson(json, sendMessage);    
  server.send(200, "application/json", sendMessage);
}

void getHorariosDeIrrigacao()
{  
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "GET");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  String agenda = "";
  serializeJson(HORARIOS, agenda);  
  server.send(200, "application/json", agenda);
}


void setHoraDoRelogio(){
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 
  
  String hora = server.arg("hora");
  String minuto = server.arg("minuto");
  String segundo = server.arg("segundo");
  rtc.setTime(hora.toInt(), minuto.toInt(), segundo.toInt());

  DynamicJsonDocument json(200); 
  json["sucesso"] = true;
  json["msg"] = rtc.getTimeStr();
  String sendMessage = "";
  serializeJson(json, sendMessage);  
  
  server.send(200, "application/json", sendMessage);  
}

void setHorariosDeIrrigacao()
{
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  
  String agenda = server.arg("agenda");  
  doc.clear(); //limpando o array;
  DeserializationError error = deserializeJson(doc, agenda);
  if (error){
    setarHorarioPadrao();
    DynamicJsonDocument json(200); 
    json["sucesso"] = false;
    json["msg"] = "JSON inválido";
    String sendMessage = "";
    serializeJson(json, sendMessage);    
    server.send(400, "application/json", sendMessage);      
  }else{    
    boolean gravou = gravarHorariosNoArquivo();
    DynamicJsonDocument json(200); 
    json["sucesso"] = gravou;
    json["msg"] = (gravou) ? "Sucesso ao gravar agenda" : "Falha ao gravar a agenda";
    String sendMessage = "";
    serializeJson(json, sendMessage);    
    server.send(200, "application/json", sendMessage);        
  }
  
}